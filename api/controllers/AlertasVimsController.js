/**
 * AlertasVimsController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */


var moment = require('moment');

module.exports = {

  getAlerts:function(req,res){
    //let query=`select * from alertasvim WHERE tiem_suceso BETWEEN '${req.param('tiem_inicio')}'::timestamp AND '${req.param('tiem_fin')}'::timestamp and coordenadax between 100000 and 1000000;`;
    let query=`select * from alertasvim WHERE tiem_suceso BETWEEN '${req.param('tiem_inicio')}'::timestamp AND '${req.param('tiem_fin')}';`;
    console.log(query);
    Vista.getDatastore().sendNativeQuery(query,function(err,data){
      if (data === undefined) return res.notFound();
      if (err) return next(err);
      return res.ok(data.rows);
    })
  },
  getZones:function(req,res){
    let query="select * from ta_zonas where tiem_elimin is null;";

    Vista.getDatastore().sendNativeQuery(query,function(err,data){
      if (data === undefined) return res.notFound();
      if (err) return next(err);
      return res.ok(data.rows);
    })
  },

  getTramos:function(req,res){
    let query="select * from tramosvim";
    Vista.getDatastore().sendNativeQuery(query,function(err,data){
      if (data === undefined) return res.notFound();
      if (err) return next(err);
      return res.ok(data.rows);
    })
  },

  getYears:function(req,res){
    let query="select distinct anio as id,anio as value from alertasvim order by anio desc;";
    Vista.getDatastore().sendNativeQuery(query,function(err,data){
      if (data === undefined) return res.notFound();
      if (err) return next(err);
      return res.ok(data.rows);
    })
  },

  getMonths:function(req,res){
    let query=`select distinct mes_num as id , mes as value from alertasvim where anio=${req.param('year')} order by mes_num asc;`;
    Vista.getDatastore().sendNativeQuery(query,function(err,data){
      if (data === undefined) return res.notFound();
      if (err) return next(err);
      return res.ok(data.rows);
    })
  },

  getWeeks:function(req,res){
    let query=`select distinct week_num as id,week_num as value from alertasvim where anio=${req.param('year')} and mes_num=${req.param('month')}  order by week_num asc;`;
    Vista.getDatastore().sendNativeQuery(query,function(err,data){
      if (data === undefined) return res.notFound();
      if (err) return next(err);
      return res.ok(data.rows);
    })
  },

  getDays:function(req,res){
    let query=`select distinct dia as id,dia as value from alertasvim where anio=${req.param('year')} and mes_num=${req.param('month')}  and week_num=${req.param('week')} order by dia asc;`;
    Vista.getDatastore().sendNativeQuery(query,function(err,data){
      if (data === undefined) return res.notFound();
      if (err) return next(err);
      return res.ok(data.rows);
    })
  },

  getShifts:function(req,res){
    let query=`select distinct turno as id,turno as value from alertasvim where anio=${req.param('year')} and mes_num=${req.param('month')} and week_num=${req.param('week')} and dia=${req.param('day')} order by turno asc;`;
    Vista.getDatastore().sendNativeQuery(query,function(err,data){
      if (data === undefined) return res.notFound();
      if (err) return next(err);
      return res.ok(data.rows);
    })
  },

  getByTime:function(req,res){

    let query='';

    if(req.param('year')){
      query=`select mes_num,mes as category,nombre_alerta,count(*) as total from alertasvim A where anio=${req.param('year')} 
             group by mes_num,mes,nombre_alerta order by mes_num asc`;

    }else{
      query=`select mes_num,mes as category,nombre_alerta,count(*) as total from alertasvim A where anio=${moment().format('YYYY')}
             group by mes_num,mes,nombre_alerta order by mes_num asc`;
    }

    if(req.param('month')){
      query=`select week_num as category,nombre_alerta,count(*) as total from alertasvim A where anio=${req.param('year')}  and mes_num=${req.param('month')}  
              group by week_num,nombre_alerta order by week_num asc`;
    }

    if(req.param('week')){
      query=`select dia as category,nombre_alerta,count(*) as total from alertasvim A where anio=${req.param('year')}   and week_num=${req.param('week')}
              group by dia,nombre_alerta order by dia asc`;
    }

    if(req.param('day')){
      query=`select turno as category,nombre_alerta,count(*) as total from alertasvim A where anio=${req.param('year')}  and week_num=${req.param('week')} and dia=${req.param('day')}
              group by turno,nombre_alerta order by turno asc`;
    }

    if(req.param('shift')){
      query=`select hora as category,nombre_alerta,count(*) as total from alertasvim A where anio=${req.param('year')}   and week_num=${req.param('week')} and dia=${req.param('day')} and turno='${req.param('shift')}'
              group by hora,nombre_alerta order by hora asc`;
    }

    console.log(query);

    Vista.getDatastore().sendNativeQuery(query,function(err,data){
      if (data === undefined) return res.notFound();
      if (err) return next(err);
      return res.ok(data.rows);
    })

  },

  getByOperator :function(req,res){

    let query=`select id_operador,nombre||' '||apellidop||' '||apellidom as category,nombre_alerta,count(*) as total from alertasvim A where `;

    if(req.param('year')){
      query+=`anio=${req.param('year')} `;

      if(req.param('month')){

        query+=`and mes_num=${req.param('month')} `;

        if(req.param('week')){

          query=` select id_operador,nombre||' '||apellidop||' '||apellidom as category,nombre_alerta,count(*) as total from alertasvim A where anio=${req.param('year')} and week_num=${req.param('week')} `;

          if(req.param('day')){

            query=` select id_operador,nombre||' '||apellidop||' '||apellidom as category,nombre_alerta,count(*) as total from alertasvim A where anio=${req.param('year')} and week_num=${req.param('week')}  and dia=${req.param('day')} `;

            if(req.param('shift')){

              query=` select id_operador,nombre||' '||apellidop||' '||apellidom as category,nombre_alerta,count(*) as total from alertasvim A where anio=${req.param('year')} and week_num=${req.param('week')}  and dia=${req.param('day')}  and turno='${req.param('shift')}' `;
            }
          }
        }
      }

    }else{
      query+=`anio=${moment().format('YYYY')}`;
    }

    query+=`group by id_operador,nombre,apellidop,apellidom,nombre_alerta order by id_operador asc`;

    Vista.getDatastore().sendNativeQuery(query,function(err,data){
      if (data === undefined) return res.notFound();
      if (err) return next(err);
      return res.ok(data.rows);
    })

  },

  getByEquipment:function(req,res){

    let query=`select nombre_equipo as category,nombre_alerta,count(*) as total from alertasvim A where  `;

    if(req.param('year')){
      query+=`anio=${req.param('year')} `;

      if(req.param('month')){

        query+=`and mes_num=${req.param('month')}  `;

        if(req.param('week')){

          query=`select nombre_equipo as category,nombre_alerta,count(*) as total from alertasvim A where anio=${req.param('year')} and week_num=${req.param('week')} `;

          if(req.param('day')){

            query=`select nombre_equipo as category,nombre_alerta,count(*) as total from alertasvim A where anio=${req.param('year')} and week_num=${req.param('week')} and dia=${req.param('day')} `;

            if(req.param('shift')){

              query=`select nombre_equipo as category,nombre_alerta,count(*) as total from alertasvim A where anio=${req.param('year')} and week_num=${req.param('week')} and dia=${req.param('day')} and turno='${req.param('shift')}' `;
            }
          }
        }
      }

    }else{
      query+=`anio=${moment().format('YYYY')}`;
    }

    query+=`group by nombre_equipo,nombre_alerta order by nombre_equipo asc`;

    Vista.getDatastore().sendNativeQuery(query,function(err,data){
      if (data === undefined) return res.notFound();
      if (err) return next(err);
      return res.ok(data.rows);
    })
  },

};

